package com.poc.authserver.usuario;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {
  @GetMapping("/me")
  public Map<String, String> validar(Principal principal) {
    Map<String, String> map = new HashMap<>();
   
    map.put("id", "1");
    map.put("name", principal.getName());
    
    return map;
  }
}
