package com.poc.foto;

import org.springframework.data.repository.CrudRepository;

public interface FotoRepository extends CrudRepository<Foto, Integer>{

}
